using System;
using UnityEngine;
#if UNITY_EDITOR
using UnityEngine.InputSystem;
#endif

public class TextCollider : MonoBehaviour
{
	[SerializeField] private string textToSpawn = "text here";
	[Range(1, 10)] [SerializeField] private float textTime = 2;

	private void OnTriggerEnter(Collider other)
	{
		UI.UIScreenController.AddData<UI.GameplayUIScreen, (string, float)>((textToSpawn, textTime));
	}

// #if UNITY_EDITOR
// 	private void Update()
// 	{
// 		var keyboard = Keyboard.current;
// 		if (keyboard.f6Key.wasPressedThisFrame)
// 		{
// 			OnTriggerEnter(null);
// 		}
// 	}
// #endif
}