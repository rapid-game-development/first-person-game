using System;
using UnityEngine;
#if UNITY_EDITOR
using UnityEngine.InputSystem;
#endif

public class InteractiveText : MonoBehaviour, IInteract
{
	[SerializeField] private QuestData questData;
	[SerializeField] private string textToSpawn = "text here";
	[Range(1, 10)] [SerializeField] private float textTime = 2;
	[SerializeField] private int entryId = -1;

	public void Interact()
	{
		UI.UIScreenController.AddData<UI.GameplayUIScreen, (string, float)>((textToSpawn, textTime));
		if (entryId < 0 || entryId >= questData.Objects.Length)
		{
			Debug.LogError($"Error on entryId: {entryId} in {gameObject.name}", this);
			return;
		}

		questData.Objects[entryId] = true;
	}
}