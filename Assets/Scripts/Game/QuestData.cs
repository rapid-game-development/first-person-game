using UnityEngine;


[CreateAssetMenu(fileName = "QuestData", menuName = "Game/QuestData", order = 0)]
public class QuestData : ScriptableObject
{
	[SerializeField] private bool[] objects = new bool[14];
	public bool[] Objects => objects;

	public void Reset()
	{
		for (int i = 0; i < objects.Length; ++i)
		{
			objects[i] = false;
		}
	}
}