﻿using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace UI
{
	public class MainMenuUIScreen : UIScreen
	{
		[SerializeField] private TMP_Text tx;

		protected override void OnEnable()
		{
			base.OnEnable();
			Cursor.lockState = CursorLockMode.None;
			Time.timeScale = 1f;
			// _currentMap.SwitchCurrentActionMap("UI");
		}

		public void ButtonPlay()
		{
			// SceneManager.LoadScene(2, LoadSceneMode.Single);
			// UIScreenController.ShowScreen<GameplayUIScreen>();
			UIScreenController.ShowScreen<StoryTellingUIScreen>();
		}

		public void ButtonControls()
		{
			// UIScreenController.ShowScreen<ControlsUIScreen>();
		}

		public void ButtonQuit()
		{
			Application.Quit();
		}
	}
}