using System.Collections;
using TMPro;
using UnityEngine;

namespace UI
{
	public class GameplayUIScreen : UIScreen, IDataReceiver<(string, float)>
	{
		[SerializeField] private GameObject textPanel;
		[SerializeField] private TMP_Text textData;
		private Coroutine _coroutine;

		protected override void OnEnable()
		{
			base.OnEnable();
			Time.timeScale = 1;
			Cursor.lockState = CursorLockMode.Locked;
		}

		public void SetData((string, float) item)
		{
			StopAllCoroutines();
			textPanel.SetActive(true);
			var (text, time) = item;
			textData.text = text;
			textData.alpha = 255.0f;
			_coroutine = StartCoroutine(FadeAfter(time));
		}

		private IEnumerator FadeAfter(float awaitTime)
		{
			yield return new WaitForSeconds(awaitTime);
			for (float ft = 255f; ft >= 0; ft -= 6.1125f)
			{
				textData.alpha = ft;
				yield return new WaitForSeconds(0.1f);
			}

			textPanel.SetActive(false);
			yield return null;
		}
	}
}