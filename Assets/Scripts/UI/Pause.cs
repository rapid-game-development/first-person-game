using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.SceneManagement;

namespace UI
{
	public class Pause : UIScreen 
	{
		[SerializeField] private QuestData questData;
		[SerializeField] private GameObject[] hintInBook;
		[SerializeField] private GameObject[] bookPagesPanels;

		private int _storyIndex = 0;
		private PlayerInput _playerInput;

		protected override void OnEnable()
		{
			base.OnEnable();
			Time.timeScale = 0;
			for (int i = 0; i < hintInBook.Length; i++)
			{
				hintInBook[i].SetActive(questData.Objects[i]);
			}

			Cursor.lockState = CursorLockMode.None;
		}

		public void NextBookPanel()
		{
			if (_storyIndex + 1 == bookPagesPanels.Length) return;

			bookPagesPanels[_storyIndex].SetActive(false);
			bookPagesPanels[++_storyIndex].SetActive(true);
		}

		public void PreviousBookPanel()
		{
			if (_storyIndex == 0) return;

			bookPagesPanels[_storyIndex].SetActive(false);
			bookPagesPanels[--_storyIndex].SetActive(true);
		}

		public void Resume()
		{
			UIScreenController.ShowScreen<GameplayUIScreen>();
		}
	}
}