using UnityEngine;
using UnityEngine.SceneManagement;

namespace UI
{
	public class StoryTellingUIScreen : UIScreen
	{
		[SerializeField] private QuestData questData;
		[SerializeField] private GameObject[] storyPanels;
		private int storyIndex = 0;

		public void NextStoryPanel()
		{
			if (storyIndex+1 == storyPanels.Length)
			{
				storyIndex = 0;
				UIScreenController.ShowScreen<GameplayUIScreen>();
				SceneManager.LoadScene(2, LoadSceneMode.Single);
				questData.Reset();
				return;
			}

			storyPanels[storyIndex].SetActive(false);
			storyPanels[++storyIndex].SetActive(true);
		}

		public void PreviousStoryPanel()
		{
			if (storyIndex == 0)
			{
				UIScreenController.ShowScreen<MainMenuUIScreen>();
				return;
			}
			storyPanels[storyIndex].SetActive(false);
			storyPanels[--storyIndex].SetActive(true);
		}
	}
}