using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovementBehavior : MonoBehaviour
{
	[SerializeField] private Rigidbody rigidBody;
	[SerializeField] private Animator anim;
	[SerializeField] private Transform cameraTransform;

	[Range(0.25f, 5f)] [SerializeField] private float moveSpeed = 1f;
	[Range(1, 10)] [SerializeField] private float acceleration = 5f;
	[Range(1.2f, 2f)] [SerializeField] private float sprintSpeedMult = 1.5f;
	[Range(0.5f, 1f)] [SerializeField] private float crouchSpeedMult = 0.85f;
	[SerializeField] private float jumpSpeed = 20f;
	[SerializeField] private ForceMode forceMode = ForceMode.Acceleration;
	[SerializeField] private float castOffset = 0.25f;
	[SerializeField] private LayerMask groundLayer;


	//Stored Values
	private Vector2 _movementDirection;
	private bool _jumpPressed;


	private void Awake()
	{
	}

	public void UpdateMovementData(Vector2 input)
	{
		_movementDirection = input;
	}

	void FixedUpdate()
	{
		ApplyPhysics();
	}

	private void ApplyPhysics()
	{
		// Movement Physics
		var mass = rigidBody.mass;
		var raycastPos = transform.position;
		raycastPos += transform.forward * castOffset;
		raycastPos += Vector3.up * 0.25f;
		Ray downRaw = new Ray(raycastPos, Vector3.down);

		if (!Physics.Raycast(downRaw, out var hit, Mathf.Infinity, groundLayer))
		{
#if UNITY_EDITOR
			Debug.LogError("Player out of map, no ground found");
#endif
			return;
		}

		var groundNormal = hit.normal;
		var right = cameraTransform.right;
		var forward = Vector3.Cross(-groundNormal, right);
		Debug.DrawRay(transform.position, forward * 3f, Color.red);

		var currentVel = rigidBody.velocity;
		var targetVel = (forward * _movementDirection.y + right * _movementDirection.x).normalized * moveSpeed;
		targetVel.y = currentVel.y;
		var force = (targetVel - currentVel) * acceleration * mass;
		rigidBody.AddForce(force, forceMode);

		if (_jumpPressed && hit.distance <= 0.26f)
		{
			// anim.SetTrigger("Jump");
			rigidBody.AddForce(Vector3.up * jumpSpeed * mass, ForceMode.Impulse);
		}

		_jumpPressed = false; // Consumed Jump Button Press
	}

	public void TryJump()
	{
		_jumpPressed = true;
	}
}