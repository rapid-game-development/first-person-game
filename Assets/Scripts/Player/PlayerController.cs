using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.EventSystems;
using UnityEngine.InputSystem.Interactions;

public class PlayerController : MonoBehaviour
{
	[SerializeField] private PlayerInput playerInput;
	[SerializeField] private MovementBehavior playerMovement;
	[SerializeField] private InteractionBehavior interactionBehavior;
	[SerializeField] private CameraBehavior playerCamera;


	private void Awake()
	{
		if (playerMovement == null) playerMovement = GetComponent<MovementBehavior>();
	}

	public void InteractionButton(InputAction.CallbackContext context)
	{
		if (context.started)
		{
			interactionBehavior.Interact();
		}
	}

	public void Move(InputAction.CallbackContext value)
	{
		var input = value.ReadValue<Vector2>();
		playerMovement.UpdateMovementData(input);
	}

	public void Look(InputAction.CallbackContext value)
	{
		var input = value.ReadValue<Vector2>();
		playerCamera.UpdateLookData(input);
	}

	public void Sprint(InputAction.CallbackContext context)
	{
		if (context.started)
		{
		}

		if (context.canceled)
		{
		}
	}

	public void Crouch(InputAction.CallbackContext context)
	{
		if (context.started)
		{
		}
	}

	public void Jump(InputAction.CallbackContext context)
	{
		if (context.started)
		{
			playerMovement.TryJump();
		}
	}

	public void Pause(InputAction.CallbackContext value)
	{
		if (value.started)
		{
			UI.UIScreenController.ShowScreen<UI.Pause>();
		}
	}
}