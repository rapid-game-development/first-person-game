using System;
using Unity.VisualScripting;
using UnityEngine;


public class CameraBehavior : MonoBehaviour
{
	[SerializeField] private Transform cameraTransform;
	[Range(0.01f, 100)] [SerializeField] private float sensitivity = 0.5f;
	private Vector2 _lookInput = Vector2.zero;
	[SerializeField] private float maxAngle = 150;
	[SerializeField] private float minAngle = 50;

#if UNITY_EDITOR
	private void Start()
	{
		Cursor.lockState = CursorLockMode.Locked;
	}
#endif

	public void UpdateLookData(Vector2 input)
	{
		_lookInput = input * sensitivity;
	}

	private void LateUpdate()
	{
		Look();
	}

	private void Look()
	{
		if (_lookInput.sqrMagnitude == 0) return;
		var nextAngle = cameraTransform.localEulerAngles;
		var xAngle = _lookInput.y + nextAngle.x;
		xAngle = Mathf.Clamp(xAngle, minAngle, maxAngle);
		nextAngle.x = xAngle;
		nextAngle.y += _lookInput.x;
		cameraTransform.localEulerAngles = nextAngle;
	}

#if UNITY_EDITOR
	private void OnGUI()
	{
		GUILayout.BeginVertical();
		GUILayout.Label($"camera rot{cameraTransform.eulerAngles.x}- input:{_lookInput.y}");
		GUILayout.EndVertical();
	}
#endif
}