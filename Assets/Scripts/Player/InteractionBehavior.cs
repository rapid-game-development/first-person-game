using UnityEngine;

public class InteractionBehavior : MonoBehaviour
{
	[SerializeField] private LayerMask interactableLayer;
	[SerializeField] private Transform cameraTransform;
	public void Interact()
	{
		var faceRay = new Ray(cameraTransform.position,cameraTransform.forward);

		if (!Physics.Raycast(faceRay, out var hit, Mathf.Infinity, interactableLayer))
		{
			Debug.Log($"no hit on interact",this);
			return;
		}

		var interactable =hit.transform.GetComponent<InteractiveText>();
		if (interactable == null)
		{
			Debug.LogError($"item on interact layer should have an IInteract implementation");
			return;
		}
			
		interactable.Interact();
	}
}